library(shiny)


############################################################################################################
# UI
############################################################################################################
ui <- fluidPage(
  titlePanel("ACD Subsetter"
             ),
  
  sidebarLayout(
    sidebarPanel("Data Source: http://www.trussel2.com/ACD/", 
                 p("This site is NOT an official resource for research."),
                 p("This site is an UNOFFICIAL tool to reference ACD data."),
                 p(),
                 em("It is the responsibility of the user to verify the authenticity of any information found on this website.")
                 ),
    mainPanel(
      
              h1("Welcome..."), 

              p("to the Austronesian Comparative Dictionary Subsetter."),

                p("This website draws data from the Austronesian Comparative Dictionary
                to allow easy subsetting of conditioned data."),
              
              p(),
              
              h2("Features"),

              code("-RegEx (Regular Expressions) are ENABLED"),
              
              p(),
              
              code("-Currently pulling only Proto-forms")
 
              
  )
  
)

)


############################################################################################################
# SERVER
############################################################################################################

server <- function(input, output) {
  
  
}



shinyApp(ui = ui, server = server)





